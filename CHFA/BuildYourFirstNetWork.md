# 搭建示例网络

按照官方文档先把示例网络跑起来，[官方文档地址](https://hyperledger-fabric.readthedocs.io/en/release-1.4/build_network.html#)，[中文文档地址](https://hyperledger-fabric-cn.readthedocs.io/zh/latest/build_network.html)。

## 安装docker，docker-compose

```
$ sudo apt install docker.io
$ sudo apt install docker-compose
```

> 建议配置 docker 镜像加速，可以使用阿里云的 docker 镜像加速器，具体方法请百度。

## 下载所需文件

```
# Fetch bootstrap.sh from fabric repository using
curl -sS https://raw.githubusercontent.com/hyperledger/fabric/master/scripts/bootstrap.sh -o ./scripts/bootstrap.sh

# Change file mode to executable
chmod +x ./scripts/bootstrap.sh

# Download binaries and docker images
./scripts/bootstrap.sh [version] [ca version] [thirdparty_version]
```

## 启动示例网络

```
cd fabric-samples/first-network
./byfn up
```

运行成功会输出如下：

```
========= All GOOD, BYFN execution completed =========== 


 _____   _   _   ____   
| ____| | \ | | |  _ \  
|  _|   |  \| | | | | | 
| |___  | |\  | | |_| | 
|_____| |_| \_| |____/ 
```

这是使用已经配置好的脚本和配置文件自动化搭建示例网络，手动搭建网络的过程后续再补充。

## 手动执行命令

```
# 生成相关配置文件
$ cryptogen generate --config=./crypto-config.yaml

$ export FABRIC_CFG_PATH=$PWD

$ configtxgen -profile TwoOrgsOrdererGenesis -channelID byfn-sys-channel -outputBlock ./channel-artifacts/genesis.block

$ export CHANNEL_NAME=mychannel && configtxgen -profile TwoOrgsChannel -outputChannelTx ./channel-artifacts/channel.tx -channelID $CHANNEL_NAME 

$ configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/Org2MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org2MSP

# 配置环境变量
$ CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp

$ CORE_PEER_ADDRESS=peer0.org1.example.com:7051

$ CORE_PEER_LOCALMSPID="Org1MSP"

$ CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt

# 创建、加入通道

$ export CHANNEL_NAME=mychannel

$ export ORDERER_TLS_CERT=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

$ peer channel create -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifact/mychannel.tx --tls --cafile $ORDERER_TLS_CERT

$ peer channel join -b mychannel.block

# 更新锚节点

$ peer channel update -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/Org1MSPanchors.tx --tls --cafile $ORDERER_TLS_CERT

# 安装、实例化链码

# Go 语言的链码路径使用 $GOPATH/src 的相对路径
$ peer chaincode install -n mycc -v 1.0 -p github.com/chaincode/chaincode_example02/go/

# Node 和 Java 的链码路径使用绝对路径
$ peer chaincode install -n mycc -v 1.0 -l node -p /opt/gopath/src/github.com/chaincode/chaincode_example02/node/

$ peer chaincode install -n mycc -v 1.0 -l java -p /opt/gopath/src/github.com/chaincode/chaincode_example02/java/

$ peer chaincode instantiate -o orderer.example.com:7050 --tls --cafile $ORDERER_TLS_CERT -C $CHANNEL_NAME -n mycc -v 1.0 -c '{"Args":["init", "a", "100", "b", "200"]}' -P "AND ('Org1MSP.peer', 'Org2MSP.peer')"

# 调用链码

$ peer chaincode query -n mycc -C $CHANNEL_NAME -c '{"Args", ["query", "a"]}'

$ peer chaincode invoke -o orderer.example.com:7050 --tls true --cafile $ORDERER_TLS_CERT -C $CHANNEL_NAME -n mycc --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"Args":["invoke","a","b","10"]}'

```