CHFA Domains & Competencies v1.0
 

## Application Lifecycle Management – 20%

Install and Instantiate chaincode package
Configure endorsement policy
Define collection policy for private data
Modify or upgrade chaincode

## Install and Configure Network – 25%

Modify the world state database configuration
Define initial multi-org configuration policy
Configure Ordering service (Kafka)
Configure Hyperledger Fabric containers
Define network config options (block creation options, etc)
Enable TLS for communication
Generate genesis block
Configure service discovery node (e.g. peer and orderer addresses)


## Diagnostics and Troubleshooting – 15%

Query and analyse peer logs
Query and analyse CA logs
Query and analyse Orderer logs
Query and analyse chaincode logs


## Membership Service Provider – 20%

Configure ACL
Create end user identity
Revoke an identity
Configure and start Hyperledger Fabric CA
Configure Hyperledger Fabric for hardware security module


## Network Maintenance and Operations – 20%

Add a peer to existing organization
Create a channel
Add an org to a channel
Update channel configuration
Update a Hyperledger Fabric Instance